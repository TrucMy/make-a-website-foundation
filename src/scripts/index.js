import '../styles/index.scss';

$(document).ready(() => {

    console.log('Hello jQuery');

    $(function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            captions: true,
            slideWidth: 1200
        });
    });

    //COMMENTS! Followed a youtube tutorial to make this accordion
    var accordions = document.getElementsByClassName("accordion");
    for (var i = 0; i < accordions.length; i++) {
        accordions[i].onclick = function () {
            var content = this.nextElementSibling;

            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        }
    }
    /**
     * Documentation for the slider
     * https://bxslider.com/examples/auto-show-start-stop-controls/ 
     */

    /**
    * Documentation for the Lightbox - Fancybox
    * See the section 5. Fire plugin using jQuery selector.
    * http://fancybox.net/howto
    */

    /**
     * Boostrap Modal (For the Learn More button)
     * https://getbootstrap.com/docs/4.0/components/modal/
     */


});

/**
 * REMEMBER!!
 * Declaring Global functions that are accessible in your HTML.
 */
window.helloWorld = function () {
    console.log('HEllooOooOOo!');
};
